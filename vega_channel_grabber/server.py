"""
Telegram application for receiving data published in telegram channels.
Based on API telegram client.
"""
import os
import logging.config

from dotenv import load_dotenv
from telethon import TelegramClient, events

# Logging
logging.config.fileConfig(fname='logs.conf', disable_existing_loggers=True)
LOGGER = logging.getLogger('channel_grabber.server')

# Configuration
load_dotenv()

API_ID: int = int(os.getenv('API_ID'))
API_HASH: str = os.getenv('API_HASH')
TARGET_CHANNEL = int(os.getenv('TARGET_CHANNEL'))

_source_channels: str = os.getenv('CHANNELS')  # "test_channel1_id, test_channel2_id..."
CHANNELS: list = _source_channels.split(',')
for index, _raw_channel in enumerate(CHANNELS):
    CHANNELS[index] = int(_raw_channel)
    LOGGER.info('Added channel {}'.format(CHANNELS[index])), 

LOGGER.info('Target channel is {}'.format(TARGET_CHANNEL))
LOGGER.info('API APP is {} {}'.format(API_ID, API_HASH))

client = TelegramClient('grabber', API_ID, API_HASH)


@client.on(events.NewMessage(chats=CHANNELS))
@client.on(events.MessageEdited(chats=CHANNELS))
async def grab_msg_from_channel(event):
    LOGGER.info('New message in source channel')

    await client.forward_messages(TARGET_CHANNEL, event.message)
    LOGGER.info('Message forwarded to target channel')

@client.on(events.NewMessage())
async def all_msgs_callback(event):
    LOGGER.info('Got message')
    LOGGER.info(repr(event))
    LOGGER.info(str(event))

if __name__ == '__main__':
    client.start()
    LOGGER.info('Authorization complete. Channel grabber started')
    LOGGER.info("Starting asyncio loop")

    try:
        client.run_until_disconnected()
    except e:
        LOGGER.info("An exception has occured during run_until_disconnected")
        LOGGER.exception("Expection during run_until_disconnected")
        LOGGER.error(repr(e))
        raise

    LOGGER.info('Channel grabber ended')
