# vega_channel_grabber

Telegram приложение для получения данных, публикуемых в интересных пользователю telegram каналах.

## Installation

### Подготовка приложения

1) Войти в [telegram core](https://my.telegram.org/)
2) Посетить [API development tools](https://my.telegram.org/apps)
3) Нажать кнопку `Create new application` и заполнить предлагаемые поля
4) Полученные `api_id` и `api_hash` занести в файл конфигурации

### Конфигурация

Приложение конфигурируется с помощью файла переменных окружений `.env` в рабочей директории.

```env
# TG Client API параметры
# api_id и api_hash из https://my.telegram.org/apps
API_ID=...
API_HASH=...

# ID интересующих telegram каналов, перечисленные через запятую.
# Аккаунт пользователя, чьи api_id и api_hash указаны выше, 
# должен быть подписан на каждый из указанных каналов.
# Внимание! Номера каналов указываются как положительные числа - без минусов.
CHANNELS=15679876,273273832

# ID канала или tg бота куда будут пересылаться интересующие сообщения
TARGET_CHANNEL=-1001162310979 
```

## Первый запуск

При первом запуске необходимо пройти процесс авторизации в телеге.
Есть два метода --- полностью через докер или через установленный на хосте python.

### Докером

```bash
# В директории vega_mirea_department_helper

# Остановить все запущеные контейнеры
docker-compose down

# Удалить старую сессию
rm -f ./vega_channel_grabber/grabber.session

# Запустить контейнер для авторизации
docker-compose -f docker-compose.auth.yml run --rm channel-grabber-auth

# После этого надо ввести номер телефона, код подтверждения (приходит в телегу или в смс),
# и дождаться сообщения типа `Signed in successfully as <username>`
# После этого можно прибить созданный для авторизации контейнер через ctrl+c
```

### Питоном

```bash
python3 server.py
```
При первом запуске потребуется ввести номер телефон и код подтверждения.
После этого клиент будет работать без запроса телефона, а в директории проекта 
появится файл `grabber.session` - не удаляйте его.


Из официальной документации: 
```text
The session file contains enough information for you to login without re-sending the code, 
so if you have to enter the code more than once, maybe you’re changing the working directory, 
renaming or removing the file, or using random names.

These database files using sqlite3 contain the required information to talk to the Telegram servers, 
such as to which IP the client should connect, port, authorization key so that messages can be encrypted, and so on.
```

Далее можно воспользоваться docker контейнером

### Docker

___Сборка образа:___

```sh
docker build -t vega-channel-grabber ./
```

___Запуск контейнера___:

```sh
docker run \
    --name vega_grabber \
    --restart unless-stopped \
    vega-channel-grabber
```

___Войти в работающий контейнер (для отладки):___

```sh
docker exec -ti vega_grabber bash
```

___Просмотр логов___

```sh
docker logs vega_grabber
```
