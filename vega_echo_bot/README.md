[![pipeline status](https://gitlab.com/mixa2130/vega_mirea_department_helper/badges/master/pipeline.svg)](https://gitlab.com/mixa2130/vega_mirea_department_helper/-/commits/master)

# vega_echo_bot

Telegram бот для дублирования уведомлений из корпоративного telegram канала в отведённую графу сайта заказчика

## Installation

### Подготовка бота

1) Создать telegram бота через @BotFather(`/newbot`);
2) Установить Privacy mode в состояние DISABLED(`/setprivacy`);
3) Дать созданному боту права администратора в соответствующем канале.

### Конфигурация

Бот конфигурируется с помощью файла переменных окружений `.env` в рабочей директории.

Пример `.env`:

```env
# API токен бота
TG_BOT_API_TOKEN=...

# Параметры вебхука
# Публичный адрес вебхука
WEBHOOK_URL=https://example.com/webhook/path
# Параметры сервера вебхука
WEBHOOK_HOST=127.0.0.1
WEBHOOK_PATH=/
# Хост и порт для создания сервера
WEBAPP_HOST=localhost
WEBAPP_PORT=5000

# Параметры REST-api для отправки событий
REST_URL=https://example.com
REST_ENDPOINT=/path/to/api
# Токен для аутентификации
REST_TOKEN="abrakadabra"

# Имя инстанса бота
# На случай если надо крутить несколько инстансов
# и отличать их между собой
INSTANCE_NAME=echo_bot
```

### Docker

___Сборка образа:___

```sh
docker build -t vega-bot ./
```

___Запуск контейнера___:

```sh
docker run \
    --name vega_echo_bot \
    -p <host port>:<container port> \ 
    --restart unless-stopped \
    vega-bot
```

___Войти в работающий контейнер (для отладки):___

```sh
docker exec -ti vega_echo_bot bash
```

___Просмотр логов___

```sh
docker logs vega_echo_bot
```

## REST

Запрос к серверу формируется на основе данных, указанных в файле `.env`:

`{REST_URL}{REST_ENDPOINT}`

`REST_TOKEN` передаётся json полем вместе с данными

Пример HTTP запроса со стороны клиента:

```http request
POST /endpoint HTTP/1.1
Host: example.com
Content-Encoding: UTF-8
Content-Type: application/json
Content-Length: 215

{
  "chat": {
    "id": -1001162310979,
    "title": "test_vega_bot",
    "type": "channel"
  },
  "edited": false,
  "pinned_message": false,
  "message_id": 290,
  "author_nickname": "Gree",
  "text": "Умный текст",
  "date": 1616170950,
  "REST_TOKEN": "abrakadabra"
}
```

## Output data format

* `chat` — `dict` - описание канала, в который пришло сообщение:
    * `id` — `signed int` - идентификатор канала;
    * `title` — `str` - заголовок канала — как его видят другие пользователи;
    * `type` — `str` - тип — `channel`;
* `edited` — `bool` - признак редактирования сообщения (остаётся в состоянии false, если отредактированное сообщение
  было закреплено. `message_id` при этом не меняется);
* `pinned_message` — `bool` - признак закрепления сообщения (остаётся в состоянии false, если закреплённое сообщение
  было отредактировано. `message_id` при этом не меняется);
* `message_id` — `int` - идентификатор сообщения. Не меняется в случае редактирования или закрепления;
* `author_nickname` — `str` - ***optional*** - имя автора сообщения;
* `text` — `str` - ***optional*** - текст сообщения;
* `file` — `dict` - ***optional*** - прикреплённый файл:
    * `file_id` — `str` - идентификатор файла;
    * `file_type` — `str` - тип файла (text, document, photo, voice);
    * `file_size` — `int` - размер файла;
    * `file_name` — `str` - ***optional*** - имя файла;
* `date` — `int` - время отправки сообщения в виде unix timestamp;
* `forward_from` — `dict` - ***optional*** - описание чата, пользователя, .. откуда было переслано сообщение:
    * `id` — `int` - идентификатор чата/пользователя/бота;
    * `username` — `str` - ник бота, канала, пользователя для приватного чата, супергруппы, другого пользователя,
      если таковое имеется;
    * `type` — `str` -  private/group/supergroup/channel/user/bot;  
    * `title` — `str` - имя-заголовок, которое видит обычный пользователь при поиске, если таковое имеется;  
* `REST_TOKEN` — `str` - идентификационный токен.

### Ошибка при разборе сообщения

В случае ошибки — сервер получит соответствующее сообщение:

```json
{
  "channel_id": -1001162310979,
  "error": "Message parser error, please check logs",
  "REST_TOKEN": "abrakadabra"
}
```

Код ошибки, а также её описание — будут занесены в stdout logger.

В данном случае,
`"channel_id": 0`
будет свидетельствовать о получении ботом, со стороны вебхука, неправильного сообщения — 
не удовлетворяющего Telegram Bot API 5.1

### Note

Чтобы получить файл, по его `file_id`, необходимо:

1) Получить `file_path`: `https://api.telegram.org/bot<token>/getFile?file_id=<file_id>`

```bash
{
    "ok": true,
    "result": {
        "file_id": <file_id>,
        "file_unique_id": "ASSuFny4AAzebAQAB",
        "file_size": 70132,
        "file_path": "photos/file_0.jpg"
    }
}   
```

2) Получить файл: `https://api.telegram.org/file/bot<token>/<file_path>`
